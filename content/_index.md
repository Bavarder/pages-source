# Bavarder

[Codeberg](https://codeberg.org/Bavarder/Bavarder) [GitHub](https://github.com/Bavarder/Bavarder) 

Chit-Chat with an AI 

![preview of the imaginer app](https://codeberg.org/Bavarder/Bavarder/raw/branch/main/data/screenshots/preview.png)

## About

Bavarder is a french word, the definiton of Bavarder is "Parler abondamment de choses sans grande portée" (Talking a lot about things that don't matter) (Larousse) which can be translated by Chit-Chat (informal conversation about matters that are not important). For non-french speakers, Bavarder can be hard to speak, it's prounouced as [bavaʀde]. Hear [here](https://youtu.be/9Qoogwxo5YA)

## Installation

### Flathub

You can either use your GNOME Software and search for "Bavarder" or you can run

```
flatpak install io.github.Bavarder.Bavarder
```

### Others

Go to [install](/install) for a full list of all available install method.

## Help

* [OpenAI](/help/openai)
* [HuggingFace](/help/huggingface)
* [Local models](/help/local)

## Changelog

### 1.1.0

We are excited to share that Imaginer is joining Bavarder.

Imaginer's mission is to bring freedom to AI by helping people collaborate and communicate with confidence and privacy. We see a deep alignment with Bavarder's vision to build a AI app that reflect their values and protect their privacy.

We're extremely excited to accelerate this mission by joining forces with Bavarder's world-class team and we are pursuing big plans for making all of our online lives freer and more empowered.

We look forward to continuing to serve you with even more exciting updates on the horizon.

- Joke aside, Imaginer is going to be discontinued and merged into Bavarder. 
- This means, that Bavarder is now able to generate pictures from a prompt
- In this release, we added a classification of providers depending of their output type (Chat, Text, Image)
- We also fixed bugs (OpenAI and local provider) thanks to some MR.

### 1.0.0

- New UI
- Local Models support
- Export chats as text or as file
- Delete and edit chats
- Markdown support without a webview
- Command running
- And more!

## Contribute

The [GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) is applicable to this project

### Translate

[![Translation status](https://translate.codeberg.org/widgets/Bavarder/-/Bavarder/multi-auto.svg)](https://translate.codeberg.org/engage/Bavarder/)

You can translate Bavarder using [Codeberg Translate](https://translate.codeberg.org/engage/Bavarder/)

### Mirrors

- [GitHub](https://github.com/Bavarder/Bavarder)
- [Codeberg](https://codeberg.org/Bavarder/Bavarder)

## See also 

### [Imaginer : Imagine with AI](https://imaginer.codeberg.page)

A tool for generating pictures with AI (GNOME app)

- [GitHub](https://github.com/ImaginerApp/Imaginer)
- [Codeberg](https://codeberg.org/Imaginer/Imaginer)

### [Gradience: Change the look of Adwaita, with ease](https://gradienceteam.github.io)

Gradience is a tool for customizing Libadwaita applications and the adw-gtk3 theme

- [GitHub](https://github.com/GradienceTeam/Gradience)
