
## Installation

### Flatpak

#### Flathub

You can either use your GNOME Software and search for "Bavarder" or you can run

```
flatpak install io.github.Bavarder.Bavarder
```

#### From Source

Clone the repo and run `flatpak-builder`

```
git clone https://codeberg.org/Bavarder/Bavarder.git # or https://github.com/Bavarder/Bavarder.git
cd Bavarder
flatpak-builder --install --user --force-clean repo/ build-aux/flatpak/io.github.Bavarder.Bavarder.json
```

### Archlinux

Bavarder is available in the AUR. Just use your favourite aur helper!

``` 
yay -S bavarder
```

### Snap

Thanks to [Soumyadeep Ghosh](https://github.com/soumyadghosh), Bavarder is available as a [snap](https://snapcraft.io/bavarder).

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/bavarder)

Or run `sudo snap install bavarder`
