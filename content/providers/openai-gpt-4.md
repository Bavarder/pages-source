# OpenAI - GPT 4

The OpenAI API uses API keys for authentication. Visit your [API Keys page](https://platform.openai.com/account/api-keys) to retrieve the API key you'll use in your requests.

## Custom API URL

Maybe you are using a custom endpoint which is providing an OpenAI-API compatible API. For using a custom endpoint, you just need to provide the base url in the preferences. For example, if you are using [FastChat](https://github.com/lm-sys/FastChat/blob/main/docs/openai_api.md), you need to put `http://localhost:8000/v1` in Preferences > OpenAI Custom Model > API URL.