# Hugging Face - BlenderBot

A token is not necessary, it allow you to have a bigger rate limit

* Create an account on [HuggingFace](https://huggingface.co)
* Follow the [documentation](https://huggingface.co/docs/hub/security-tokens) for getting an user access token
* Use the generated token in the API key entry
