# OpenAI

The OpenAI API uses API keys for authentication. Visit your [API Keys page](https://platform.openai.com/account/api-keys) to retrieve the API key you'll use in your requests.

## Custom API URL

Maybe you are using a custom endpoint which is providing an OpenAI-API compatible API. For using a custom endpoint, you just need to provide the base url in the preferences. For example, if you are using [FastChat](https://github.com/lm-sys/FastChat/blob/main/docs/openai_api.md), you need to put `http://localhost:8000/v1` in Preferences > OpenAI Custom Model > API URL.

## Custom Model 

If you want to use a model of an OpenAI-API compatible API or from the OpenAI API which isn't available in Bavarder (available: `gpt-3.5-turbo`, `gpt-4`, `text-davinci-003`), you can use the provider callled OpenAI Custom Model and set Model to the model you want to use. For example, if you want to use `gpt-4-32k` you just have to set Model to `gpt-4-32k`

## Local model

See [this page](/help/local) of the documentation.